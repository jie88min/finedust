package com.sh.finddust.Nevigation.Setting;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sh.finddust.R;

public class AppInfoActivity extends AppCompatActivity {

    private TextView version_code;
    private String deviceVersion;
    private LinearLayout toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_info);

        version_code = (TextView) findViewById(R.id.version_code);
        toolbar = (LinearLayout) findViewById(R.id.toolbar);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        setVersion();

    }

    private void setVersion(){
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            deviceVersion = pi.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        version_code.setText(deviceVersion + " V");
    }
}
