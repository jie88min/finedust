package com.sh.finddust.Nevigation.LocationSearch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.sh.finddust.R;
import com.sh.finddust.DTO.LocationData;

import java.util.List;

import static com.sh.finddust.R.id.location_addr;


/**
 * Created by 김지민 on 2017-09-06.
 */

public class SearchListAdapter extends BaseAdapter{
    private Context context;
    private List<LocationData> locationDatas;
    private LayoutInflater mInflater;
    private setSearchList setSearchList;

    public SearchListAdapter(Context context, List<LocationData> locationDatas){
        this.context = context;
        this.locationDatas = locationDatas;
        this.mInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        setSearchList = new setSearchList();
    }

    @Override
    public int getCount() {
        return locationDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return locationDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final Context context = mInflater.getContext();
        final LocationData locations = locationDatas.get(position);

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.location_item, viewGroup, false);
        }

        setSearchList.location_addr = (TextView) convertView.findViewById(location_addr);

        setSearchList.location_addr.setText(locations.getAdress());

        return convertView;
    }



    public class setSearchList{
        TextView location_addr;
    }
}
