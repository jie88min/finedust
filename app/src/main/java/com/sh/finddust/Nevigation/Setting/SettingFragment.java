package com.sh.finddust.Nevigation.Setting;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.sh.finddust.Nevigation.Setting.ReportActivity;
import com.sh.finddust.R;
import com.sh.finddust.Widget.AppWidget;
import com.sh.finddust.Widget.LocationSearchActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private LinearLayout widget_setting;
    private LinearLayout Dimensions;
    private LinearLayout Reports;
    private LinearLayout App_info;

    private TextView widget_addr;

    public SettingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance(String param1, String param2) {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        widget_setting = (LinearLayout) getActivity().findViewById(R.id.widget_setting);
        widget_setting.setOnClickListener(this);

        Dimensions = (LinearLayout) getActivity().findViewById(R.id.Dimensions);
        Dimensions.setOnClickListener(this);

        Reports = (LinearLayout) getActivity().findViewById(R.id.Reports);
        Reports.setOnClickListener(this);

        App_info = (LinearLayout) getActivity().findViewById(R.id.App_info);
        App_info.setOnClickListener(this);

        widget_addr = (TextView) getActivity().findViewById(R.id.widget_addr);

        getData();

        setAd();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.widget_setting:
                Intent i = new Intent(getActivity(), LocationSearchActivity.class);
                startActivityForResult(i, 1);
                break;
            case R.id.Dimensions:
                Intent dimens = new Intent(getActivity(), DimensionsActivity.class);
                startActivity(dimens);
                break;
            case R.id.Reports:
                Intent report = new Intent(getActivity(), ReportActivity.class);
                startActivity(report);
                break;
            case R.id.App_info:
                Intent appinfo = new Intent(getActivity(), AppInfoActivity.class);
                startActivity(appinfo);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                Intent intent = new Intent(getActivity(), AppWidget.class);
                intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
                int ids[] = AppWidgetManager.getInstance(getActivity().getApplication()).getAppWidgetIds(new ComponentName(getActivity().getApplication(), AppWidget.class));
                intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
                getActivity().sendBroadcast(intent);

                String addr = data.getStringExtra("addr");
                widget_addr.setText(addr);
            }
        }
    }

    private void getData(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String addr = prefs.getString("Widget_ADDR", "미설정");
        widget_addr.setText(addr);
    }

    // Admob 연결
    private void setAd(){
        AdView mAdView = (AdView) getActivity().findViewById(R.id.adView_setting);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
}
