package com.sh.finddust.Nevigation.Setting;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sh.finddust.DTO.ReportData;
import com.sh.finddust.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ReportActivity extends AppCompatActivity implements View.OnClickListener{

    private LinearLayout toolbar;
    private EditText email;
    private EditText reports_message;
    private TextView cancel_button;
    private TextView send_button;

    ReportData report;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        toolbar = (LinearLayout) findViewById(R.id.toolbar);
        email = (EditText) findViewById(R.id.email);
        reports_message = (EditText) findViewById(R.id.reports_message);
        cancel_button = (TextView) findViewById(R.id.cancel_button);
        send_button = (TextView) findViewById(R.id.send_button);

        toolbar.setOnClickListener(this);
        cancel_button.setOnClickListener(this);
        send_button.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar:
                if (Cancel_Check()) {
                    CancelAlertDialog();
                } else {
                    finish();
                }
                break;
            case R.id.cancel_button:
                if (Cancel_Check()) {
                    CancelAlertDialog();
                } else {
                    finish();
                }
                break;
            case R.id.send_button:
                if (Send_Check()) {
                    QuestionAlertDialog();
                }
                break;
        }
    }

    private boolean Send_Check(){
        if (email.getText().toString().trim().equals("") || !email.getText().toString().trim().contains("@")) {
            email.setError("이메일을 확인해주세요.");
        } else if (reports_message.getText().toString().equals("")) {
            reports_message.setError("내용을 입력해주세요.");
        } else {
            return true;
        }
        return false;
    }

    private boolean Cancel_Check(){
        boolean result = false;
        if (reports_message.getText().toString().length() > 0) {
            result = true;
        }
        return result;
    }

    private String getDate(){
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return sdf.format(date);
    }

    private void QuestionAlertDialog(){
        new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("전송하시겠습니까?")
                //.setContentText("적어주신 이메일로 답변이 전송됩니다.")
                .showCancelButton(true)
                .setCancelText("취소")
                .setCancelClickListener(null)
                .setConfirmText("확인")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        HttpAsyncTask httpTask = new HttpAsyncTask();
                        httpTask.execute("http://finedust.xyz/json", email.getText().toString(), reports_message.getText().toString(), getDate().toString());
                        Log.d("report", email.getText().toString()+reports_message.getText().toString()+getDate().toString());

                        sweetAlertDialog.dismiss();
                        SendAlertDialog();
                    }
                })
                .show();
    }

    private void SendAlertDialog(){
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("문의가 접수되었습니다.")
                //.setContentText("성심 성의껏 답변해 드리도록 하겠습니다!")
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmText("확인")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        finish();
                    }
                })
                .show();
    }

    private void CancelAlertDialog(){
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("작성중인 문의 내용이 있습니다.")
                .setContentText("취소하시겠습니까?")
                .showCancelButton(true)
                .setCancelText("아니요")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .setConfirmText("네")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        finish();
                    }
                })
                .show();
    }
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            report = new ReportData();
            report.setEmail(urls[1]);
            report.setContent(urls[2]);
            report.setDate(urls[3]);

            return sendReport(urls[0], report);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    public static String sendReport(String url, ReportData report){
        InputStream is = null;
        String result = "";
        try {
            URL urlCon = new URL(url);
            HttpURLConnection httpCon = (HttpURLConnection)urlCon.openConnection();

            String json = "";

            // JSON 객체 조립
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("email", report.getEmail());
            jsonObject.accumulate("content", report.getContent());
            jsonObject.accumulate("date", report.getDate());

            // JsonObject -> String 변환
            json = jsonObject.toString();

            // ** Alternative way to convert Person object to JSON string usin Jackson Lib
            // ObjectMapper mapper = new ObjectMapper();
            // json = mapper.writeValueAsString(person);

            // Set some headers to inform server about the type of the content
            httpCon.setRequestProperty("Accept", "application/json");
            httpCon.setRequestProperty("Content-type", "application/json");

            // OutputStream으로 POST 데이터를 넘겨주겠다는 옵션.
            httpCon.setDoOutput(true);
            // InputStream으로 서버로 부터 응답을 받겠다는 옵션.
            httpCon.setDoInput(true);

            OutputStream os = httpCon.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.flush();
            // receive response as inputStream
            try {
                is = httpCon.getInputStream();
                // convert inputstream to string
                if(is != null) {
                    result = convertInputStreamToString(is);
                    Log.d("report44", result.toString());
                }else{
                    result = "Did not work!";
                    Log.d("report44" ," did not work");
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                httpCon.disconnect();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
