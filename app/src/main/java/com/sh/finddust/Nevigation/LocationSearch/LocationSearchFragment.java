package com.sh.finddust.Nevigation.LocationSearch;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.sh.finddust.DTO.ApiNearStation;
import com.sh.finddust.DTO.ApiNowDustData;
import com.sh.finddust.R;
import com.sh.finddust.Util.ClearEditText;
import com.sh.finddust.DTO.LocationData;
import com.sh.finddust.Util.CustomProgressBar;
import com.sh.finddust.Util.FavoritManager;
import com.sh.finddust.Util.GetAPIData;
import com.sh.finddust.Util.Global;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sh.finddust.Main.MainActivity.MyFavorit;
import static com.sh.finddust.Main.MainActivity.MyFavoritData;
import static com.sh.finddust.Main.MainActivity.MyFavoritStation;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LocationSearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LocationSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocationSearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ListView location_list;
    private ClearEditText edittext_location;
    private TextView empty_list;

    private SearchListAdapter adapter;

    private ArrayList<LocationData> locationDatas;
    private Document doc = null;
    private FavoritManager favoritManager;


    public LocationSearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LocationSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LocationSearchFragment newInstance(String param1, String param2) {
        LocationSearchFragment fragment = new LocationSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location_search, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        location_list = (ListView) getActivity().findViewById(R.id.location_list);
        edittext_location = (ClearEditText) getActivity().findViewById(R.id.edittext_location);
        empty_list = (TextView) getActivity().findViewById(R.id.empty_list);

        locationDatas = new ArrayList<LocationData>();
        adapter = new SearchListAdapter(getActivity(), locationDatas);
        location_list.setAdapter(adapter);


        edittext_location.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edittext_location.getWindowToken(), 0);
                        if (!v.getText().toString().trim().equals("")) {
                            new GetLocationData(v.getText().toString()).execute();
                            location_list.setEmptyView(empty_list);
                        } else {
                            v.setError("주소를 입력해 주세요.");
                        }
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });

        location_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(locationDatas.get(position).getAdress())
                        .setContentText("즐겨찾기 목록에 추가하시겠습니까?")
                        .setCustomImage(R.drawable.ic_favorit_icon)
                        .showCancelButton(true)
                        .setCancelText("취소")
                        .setCancelClickListener(null)
                        .setConfirmText("확인")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                favoritManager = new FavoritManager(getActivity(), "my_favorit", MyFavorit);
                                int check = favoritManager.CheakFavorit(locationDatas.get(position).getAdress());
                                if (check == 0) {
                                    LocationData locationData = new LocationData(
                                            locationDatas.get(position).getSidoName(),
                                            locationDatas.get(position).getSggName(),
                                            locationDatas.get(position).getUmdName(),
                                            locationDatas.get(position).getTmX(),
                                            locationDatas.get(position).getTmY());
                                    MyFavorit.add(locationData);

                                    favoritManager.setFavorit();

                                    sweetAlertDialog
                                            .setTitleText("추가되었습니다.")
                                            .showContentText(false)
                                            .setConfirmText("확인")
                                            .showCancelButton(false)
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    sweetAlertDialog.dismiss();
                                                    new Thread() {
                                                        public void run() {
                                                            try{
                                                                int lastIdx = MyFavorit.size() - 1;
                                                                GetAPIData reqestStation = new GetAPIData(Double.parseDouble(MyFavorit.get(lastIdx).getTmX()), Double.parseDouble(MyFavorit.get(lastIdx).getTmY()));

                                                                GetAPIData requestDust = new GetAPIData(reqestStation.getNearStationAPI());
                                                                ApiNowDustData dust = requestDust.addFavoritDustAPI(lastIdx);
                                                                ApiNearStation location = requestDust.getFavoritNearStation();

                                                                MyFavoritData.add(dust);
                                                                MyFavoritStation.add(location);

                                                            }catch (IOException e){
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }.start();
                                                }
                                            })
                                            .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                } else if (check == 1) {
                                    sweetAlertDialog
                                            .setTitleText("오류")
                                            .setContentText("이미 추가되어있는 장소 입니다.")
                                            .showContentText(true)
                                            .setConfirmText("확인")
                                            .showCancelButton(false)
                                            .setConfirmClickListener(null)
                                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                } else if (check == 2) {
                                    sweetAlertDialog
                                            .setTitleText("오류")
                                            .setContentText("즐겨찾기는 최대 " + favoritManager.getFavoritSize() + "개 까지 추가가능합니다.")
                                            .showContentText(true)
                                            .setConfirmText("확인")
                                            .showCancelButton(false)
                                            .setConfirmClickListener(null)
                                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                }

                            }
                        })
                        .show();
            }
        });

        setAd();
    }

    private class GetLocationData extends AsyncTask<String, Void, Document>{
        String umdName;
        CustomProgressBar customProgressBar;
        public GetLocationData (String umdName) {
            this.umdName = umdName;
            if (locationDatas != null) locationDatas.clear();
            customProgressBar = new CustomProgressBar(getActivity());
            customProgressBar.show();
        }
        @Override
        protected Document doInBackground(String... urls) {
            URL url;
            try {
                String apiKey = Global.API_KEY;
                url = new URL("http://openapi.airkorea.or.kr/openapi/services/rest/MsrstnInfoInqireSvc/getTMStdrCrdnt?" + apiKey + "&numOfRows=500&umdName=" + umdName);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                doc = db.parse(new InputSource(url.openStream()));
                doc.getDocumentElement().normalize();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return doc;
        }

        @Override
        protected void onPostExecute(Document doc) {
            NodeList nodeList = doc.getElementsByTagName("item");

            for(int i = 0; i< nodeList.getLength(); i++){

                Node node = nodeList.item(i);
                Element fstElmnt = (Element) node;

                String sidoName = fstElmnt.getElementsByTagName("sidoName").item(0).getChildNodes().item(0).getNodeValue();
                String sggName = fstElmnt.getElementsByTagName("sggName").item(0).getChildNodes().item(0).getNodeValue();
                String umdName = fstElmnt.getElementsByTagName("umdName").item(0).getChildNodes().item(0).getNodeValue();
                String tmX = fstElmnt.getElementsByTagName("tmX").item(0).getChildNodes().item(0).getNodeValue();
                String tmY = fstElmnt.getElementsByTagName("tmY").item(0).getChildNodes().item(0).getNodeValue();

                LocationData locationData = new LocationData(sidoName, sggName, umdName, tmX, tmY);
                locationDatas.add(locationData);
            }
            adapter.notifyDataSetChanged();

            customProgressBar.dismiss();

            super.onPostExecute(doc);
        }
    }

    // Admob 연결
    private void setAd(){
        AdView mAdView = (AdView) getActivity().findViewById(R.id.adView_search);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
}
