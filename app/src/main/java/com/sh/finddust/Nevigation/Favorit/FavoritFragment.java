package com.sh.finddust.Nevigation.Favorit;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.sh.finddust.R;
import com.sh.finddust.Util.FavoritManager;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sh.finddust.Main.MainActivity.MyFavorit;
import static com.sh.finddust.Main.MainActivity.MyFavoritData;
import static com.sh.finddust.Main.MainActivity.MyFavoritStation;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavoritFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavoritFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoritFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private SwipeMenuListView favorit_list;
    private TextView empty_list;
    private ImageView question_button;
    private FavoritListAdapter adapter;

    private FavoritManager favoritManager;


    public FavoritFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavoritFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoritFragment newInstance(String param1, String param2) {
        FavoritFragment fragment = new FavoritFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorit, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        favorit_list = (SwipeMenuListView) getActivity().findViewById(R.id.favorit_list);
        favorit_list.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        adapter = new FavoritListAdapter(getActivity(), MyFavorit);
        favorit_list.setAdapter(adapter);

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getActivity());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.parseColor("#19000000")));
                // set item width
                deleteItem.setWidth(dp2px(45));
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        favorit_list.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
    					delete(position);
                        break;
                }
                return false;
            }
        });

        favorit_list.setMenuCreator(creator);


        empty_list  = (TextView) getActivity().findViewById(R.id.empty_list);
        favorit_list.setEmptyView(empty_list);

        question_button = (ImageView) getActivity().findViewById(R.id.question_button);
        question_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setCustomImage(R.drawable.ic_delete)
                        .setTitleText("즐겨찾기 삭제")
                        .setContentText("항목을 왼쪽으로 드래그하면\n즐겨찾기 목록에서 삭제 할 수 있습니다.")
                        .showCancelButton(false)
                        .setConfirmText("확인")
                        .setConfirmClickListener(null)
                        .show();
            }
        });

        setAd();
    }

    // dp -> px
    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getActivity().getResources().getDisplayMetrics());
    }

    // 즐겨찾기 삭제
    private void delete(final int position) {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(MyFavorit.get(position).getAdress())
                .setContentText("즐겨찾기 목록에서 삭제하시겠습니까?")
                .showCancelButton(true)
                .setCancelText("취소")
                .setCancelClickListener(null)
                .setConfirmText("확인")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        MyFavorit.remove(position);
                        MyFavoritData.remove(position);
                        MyFavoritStation.remove(position);
                        adapter.notifyDataSetChanged();

                        favoritManager = new FavoritManager(getActivity(), "my_favorit", MyFavorit);
                        favoritManager.setFavorit();

                        sweetAlertDialog
                                .setTitleText("삭제되었습니다.")
                                .showContentText(false)
                                .setConfirmText("확인")
                                .showCancelButton(false)
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                    }
                })
                .show();
    }

    // Admob 연결
    private void setAd(){
        AdView mAdView = (AdView) getActivity().findViewById(R.id.adView_favorit);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }
}
