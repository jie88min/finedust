package com.sh.finddust.Widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.sh.finddust.DTO.ApiNearStation;
import com.sh.finddust.DTO.ApiNowDustData;
import com.sh.finddust.DTO.LocationData;
import com.sh.finddust.Nevigation.LocationSearch.SearchListAdapter;
import com.sh.finddust.R;
import com.sh.finddust.Util.ClearEditText;
import com.sh.finddust.Util.CustomProgressBar;
import com.sh.finddust.Util.GetAPIData;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import cn.pedant.SweetAlert.SweetAlertDialog;

public class LocationSearchActivity extends AppCompatActivity {

    private ClearEditText edittext_location;
    private ListView location_list;
    private TextView empty_list;

    private SearchListAdapter adapter;

    private ArrayList<LocationData> locationDatas;
    private Document doc = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search);

        edittext_location = (ClearEditText) findViewById(R.id.edittext_location);
        location_list = (ListView) findViewById(R.id.location_list);
        empty_list = (TextView) findViewById(R.id.empty_list);

        locationDatas = new ArrayList<LocationData>();
        adapter = new SearchListAdapter(this, locationDatas);
        location_list.setAdapter(adapter);

        edittext_location.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edittext_location.getWindowToken(), 0);
                        if (!v.getText().toString().trim().equals("")) {
                            new GetLocationData(v.getText().toString()).execute();
                            location_list.setEmptyView(empty_list);
                        } else {
                            v.setError("주소를 입력해 주세요.");
                        }
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });

        location_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, final long id) {
                new SweetAlertDialog(LocationSearchActivity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText(locationDatas.get(position).getAdress())
                        .setContentText("선택한 장소가 맞나요?")
                        .setCustomImage(R.drawable.ic_favorit_icon)
                        .showCancelButton(true)
                        .setCancelText("취소")
                        .setCancelClickListener(null)
                        .setConfirmText("확인")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(final SweetAlertDialog sweetAlertDialog) {
                                WidgetData widgetData = new WidgetData(locationDatas.get(position).getUmdName(), locationDatas.get(position).getTmX(), locationDatas.get(position).getTmY());
                                setLocationDatas("Widget_", widgetData);

                                sweetAlertDialog.dismiss();
                                Intent intent = new Intent();
                                intent.putExtra("addr", locationDatas.get(position).getUmdName());
                                setResult(Activity.RESULT_OK, intent);
                                finish();
                            }
                        })
                        .show();
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    private class GetLocationData extends AsyncTask<String, Void, Document> {
        String umdName;
        CustomProgressBar customProgressBar;
        public GetLocationData (String umdName) {
            this.umdName = umdName;
            if (locationDatas != null) locationDatas.clear();
            customProgressBar = new CustomProgressBar(LocationSearchActivity.this);
            customProgressBar.show();
        }
        @Override
        protected Document doInBackground(String... urls) {
            URL url;
            try {
                url = new URL("http://openapi.airkorea.or.kr/openapi/services/rest/MsrstnInfoInqireSvc/getTMStdrCrdnt?ServiceKey=Ho7rFGmoIKzfvdnP1NKPhOK%2F0tLJs6Cu9XntYeb%2BKsUigQ9IV5YJNyrXY2wwmKK10pkEPv8F%2FO4GP%2F%2FAwkPrDw%3D%3D&numOfRows=500&umdName=" + umdName);
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                doc = db.parse(new InputSource(url.openStream()));
                doc.getDocumentElement().normalize();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return doc;
        }

        @Override
        protected void onPostExecute(Document doc) {
            NodeList nodeList = doc.getElementsByTagName("item");

            for(int i = 0; i< nodeList.getLength(); i++){

                Node node = nodeList.item(i);
                Element fstElmnt = (Element) node;

                String sidoName = fstElmnt.getElementsByTagName("sidoName").item(0).getChildNodes().item(0).getNodeValue();
                String sggName = fstElmnt.getElementsByTagName("sggName").item(0).getChildNodes().item(0).getNodeValue();
                String umdName = fstElmnt.getElementsByTagName("umdName").item(0).getChildNodes().item(0).getNodeValue();
                String tmX = fstElmnt.getElementsByTagName("tmX").item(0).getChildNodes().item(0).getNodeValue();
                String tmY = fstElmnt.getElementsByTagName("tmY").item(0).getChildNodes().item(0).getNodeValue();

                LocationData locationData = new LocationData(sidoName, sggName, umdName, tmX, tmY);
                locationDatas.add(locationData);
            }
            adapter.notifyDataSetChanged();

            customProgressBar.dismiss();

            super.onPostExecute(doc);
        }
    }

    public void setLocationDatas(final String key, WidgetData data) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(key + "TMX", data.getTmX());
        editor.putString(key + "TMY", data.getTmY());
        editor.putString(key + "ADDR", data.getUmdName());

        editor.apply();
    }
}
