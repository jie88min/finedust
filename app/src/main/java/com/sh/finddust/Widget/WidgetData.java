package com.sh.finddust.Widget;

/**
 * Created by 김지민 on 2017-12-13.
 */

public class WidgetData {
    private String umdName;     // 읍면동 이름
    private String tmX;
    private String tmY;

    public WidgetData(String umdName, String tmX, String tmY) {
        this.umdName = umdName;
        this.tmX = tmX;
        this.tmY = tmY;
    }

    public String getUmdName() {
        return umdName;
    }

    public void setUmdName(String umdName) {
        this.umdName = umdName;
    }

    public String getTmX() {
        return tmX;
    }

    public void setTmX(String tmX) {
        this.tmX = tmX;
    }

    public String getTmY() {
        return tmY;
    }

    public void setTmY(String tmY) {
        this.tmY = tmY;
    }


}
