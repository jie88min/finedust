package com.sh.finddust.Widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.sh.finddust.DTO.ApiNearStation;
import com.sh.finddust.DTO.ApiNowDustData;
import com.sh.finddust.R;
import com.sh.finddust.Splash.SplashActivity;
import com.sh.finddust.Util.ClassificationValue;
import com.sh.finddust.Util.FaceIcon;
import com.sh.finddust.Util.GetAPIData;
import com.sh.finddust.Util.Global;
import com.sh.finddust.Util.ParseJSON;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Implementation of App Widget functionality.
 */
public class AppWidget extends AppWidgetProvider {

    private static final String ADDR_KEY = "Widget_";
    private static String ADDR = "";

    private static CharSequence addr;
    private static CharSequence status;
    private static CharSequence value10pm;
    private static int statusIcon;

    private static AppWidgetManager mappWidgetManager;
    private static int mappWidgetIds;

    public static int SupportStation_WIDGET = 0;

    private static final String ACTION_UPDATE = "android.appwidget.action.APPWIDGET_UPDATE";


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        updateAppWidget(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    // 데이터 업데이트
    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int[] appWidgetId) {
        for (int i = 0; i < appWidgetId.length; i++){
            new Loading(context, appWidgetManager, appWidgetId[i]).execute();
        }
    }


    // 네트워크 체크
    public static boolean NetworkConnection(Context context){
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isMobileConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        boolean isWifiAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
        boolean isWifiConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        if((isWifiAvailable && isWifiConnect) || (isMobileConnect)) {
            return true;
        } else {
            return false;
        }
    }


    // 측정값 로드
    public static class Loading extends AsyncTask<Void, Void, Void>
    {

        RemoteViews views;
        Context mcontext;
        boolean addedAddr = true;
        boolean network = true;

        public Loading(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
            mcontext = context;
            mappWidgetManager = appWidgetManager;
            mappWidgetIds = appWidgetId;
        }
        @Override
        protected void onPreExecute(){
            views = new RemoteViews(mcontext.getPackageName(), R.layout.app_widget);
            views.setViewVisibility(R.id.pmView, View.GONE);
            views.setViewVisibility(R.id.widget_update_bt, View.VISIBLE);

            views.setTextViewText(R.id.tvDesc1, "");
            views.setTextViewText(R.id.tvDesc2, "");
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ArrayList<ApiNowDustData> nowLocationData;
            ArrayList<ApiNearStation> nowLocationStation;

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mcontext);

            addr = prefs.getString(ADDR_KEY + "ADDR", "");
            String tmX = prefs.getString(ADDR_KEY + "TMX", "");
            String tmY = prefs.getString(ADDR_KEY + "TMY", "");

            Log.d("Widget", "ADDRs: " + addr + ", tmX: " + tmX + ", tmY: " + tmY);

            if (NetworkConnection(mcontext)) {
                if (!addr.equals("") && !tmX.equals("") && !tmY.equals("")) {
                    try {
                        GetAPIData reqestStation = new GetAPIData(Double.valueOf(tmX), Double.valueOf(tmY));
                        nowLocationStation = reqestStation.getNearStationAPI();

                        nowLocationData = getNowDustWidget(nowLocationStation);

                        FaceIcon faceIcon = new FaceIcon();
                        ClassificationValue grade = new ClassificationValue();

                        statusIcon = faceIcon.getIcon(grade.classficationPM10(nowLocationData.get(SupportStation_WIDGET).getPm10Value()));
                        status = grade.classficationPM10(nowLocationData.get(SupportStation_WIDGET).getPm10Value());
                        value10pm = nowLocationData.get(SupportStation_WIDGET).getPm10Value() + "pm";
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    addedAddr = false;
                }
            } else {
                network = false;
            }

            return null;
        }

        @Override
        public void onPostExecute(Void avoid){
            super.onPreExecute();
            // Wiget Draw
            if (addedAddr && network) {
                views.setViewVisibility(R.id.pmView, View.VISIBLE);
                views.setViewVisibility(R.id.widget_update_bt, View.GONE);

                // Construct the RemoteViews object
                views.setTextViewText(R.id.tvDesc1, addr);
                String str = status + " (" + value10pm + ")";
                views.setTextViewText(R.id.tvDesc2, str);
                views.setImageViewResource(R.id.status_icon, statusIcon);

                Intent updates = new Intent(mcontext, AppWidget.class);
                updates.setAction(ACTION_UPDATE);
                PendingIntent updatepandding = PendingIntent.getBroadcast(mcontext, 0, updates, 0);
                views.setOnClickPendingIntent(R.id.status_icon, updatepandding);
            } else {
                views.setViewVisibility(R.id.pmView, View.GONE);
                views.setViewVisibility(R.id.widget_update_bt, View.VISIBLE);

                if (!addedAddr) {
                    Intent intent = new Intent(mcontext, LocationSearchActivity.class);
                    PendingIntent setActivity = PendingIntent.getActivity(mcontext, 0, intent, 0);
                    views.setOnClickPendingIntent(R.id.widget_update_bt, setActivity);
                } else if (!network) {
                    Intent updates = new Intent(mcontext, AppWidget.class);
                    updates.setAction(ACTION_UPDATE);
                    PendingIntent updatepandding = PendingIntent.getBroadcast(mcontext, 0, updates, 0);
                    views.setOnClickPendingIntent(R.id.widget_update_bt, updatepandding);
                }

            }

            // Instruct the widget manager to update the widget
            mappWidgetManager.updateAppWidget(mappWidgetIds, views);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        final String action = intent.getAction();
        if (action != null) {
            if (action.equals(ACTION_UPDATE)) {
                Log.d("onReceive", "ACTION_UPDATE");
                AppWidgetManager manager = AppWidgetManager.getInstance(context);
                updateAppWidget(context, manager, manager.getAppWidgetIds(new ComponentName(context, getClass())));
            }
        }
    }

    // 측정소 실시간 미세먼지 정보 API - Widget
    public static ArrayList<ApiNowDustData> getNowDustWidget(ArrayList<ApiNearStation> station) throws IOException {
        StringBuffer jsonHtml;
        ArrayList<ApiNowDustData> dustData = null;
        InputStream uis = null;
        BufferedReader br = null;
        String apiRealTimeDust = "http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty?&dataTerm=DAILY&pageNo=1&numOfRows=10";
        String stationName ="&stationName="; // url 요청 측정소 명
        String apiKey = Global.API_KEY;
        String version = "&ver=1.3";
        String type = "&_returnType=json";

        try {
            for (int i = 0; i < station.size(); i++){
                URL u = new URL(apiRealTimeDust + stationName + URLEncoder.encode(station.get(i).getStationName(), "UTF-8")+ apiKey + version + type);
                uis = u.openStream();
                br = new BufferedReader(new InputStreamReader(uis, "UTF-8"));

                jsonHtml = new StringBuffer();
                String line;

                while ((line = br.readLine()) != null) {
                    jsonHtml.append(line + "\r\n");
                }

                ParseJSON parse = new ParseJSON();
                dustData = parse.parseNowDustData(jsonHtml);
                if (!dustData.isEmpty()){
                    if (!dustData.get(i).getPm10Value().isEmpty() && !dustData.get(i).getPm10Value().equals("") && !dustData.get(i).getPm10Value().equals("-")) {
                        SupportStation_WIDGET = i;
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(br != null)
                br.close();
            if(uis != null)
                uis.close();
        }

        return dustData;
    }
}

