package com.sh.finddust.DTO;

import java.io.Serializable;

/**
 * Created by 김지민 on 2017-12-06.
 */

public class LocationData implements Serializable{
    private String sidoName;    // 시명
    private String sggName;     // 도명
    private String umdName;     // 읍면동명
    private String tmX;         // TmX 좌표
    private String tmY;         // TmY 좌표

    public LocationData(){}

    public LocationData(String sidoName, String sggName, String umdName, String tmX, String tmY) {
        this.sidoName = sidoName;
        this.sggName = sggName;
        this.umdName = umdName;
        this.tmX = tmX;
        this.tmY = tmY;
    }

    public String getSidoName() {
        return sidoName;
    }

    public void setSidoName(String sidoName) {
        this.sidoName = sidoName;
    }

    public String getSggName() {
        return sggName;
    }

    public void setSggName(String sggName) {
        this.sggName = sggName;
    }

    public String getUmdName() {
        return umdName;
    }

    public void setUmdName(String umdName) {
        this.umdName = umdName;
    }

    public String getTmX() {
        return tmX;
    }

    public void setTmX(String tmX) {
        this.tmX = tmX;
    }

    public String getTmY() {
        return tmY;
    }

    public void setTmY(String tmY) {
        this.tmY = tmY;
    }

    public String getAdress(){
        return sidoName + " " + sggName + " " + umdName;
    }

    public String getSido() { return sggName + " " + umdName;}
}
