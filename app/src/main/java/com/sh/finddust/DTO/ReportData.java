package com.sh.finddust.DTO;

/**
 * Created by 김지민 on 2017-12-17.
 */

public class ReportData {
    private String email;
    private String content;
    private String date;
    private int read;

    public ReportData() {}

    public ReportData(String email, String content, String date, int read) {
        this.email = email;
        this.content = content;
        this.date = date;
        this.read = read;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }
}
