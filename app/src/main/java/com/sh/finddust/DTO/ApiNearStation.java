package com.sh.finddust.DTO;

import java.io.Serializable;

/**
 * Created by 이창훈 on 2017-12-07.
 */

public class ApiNearStation implements Serializable {
    private String stationName;     // 측정소 명
    private String addr;            // 측정소 주소
    private double tm;              // 측정소 거리

    public ApiNearStation(){}

    public ApiNearStation(String stationName, String addr, double tm) {
        this.stationName = stationName;
        this.addr = addr;
        this.tm = tm;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public double getTm() {
        return tm;
    }

    public void setTm(double tm) {
        this.tm = tm;
    }
}
