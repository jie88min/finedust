package com.sh.finddust.DTO;

/**
 * Created by 이창훈 on 2017-12-07.
 */

public class GeoTransPoint {
    public double x;
    public double y;
    public double z;
    /**
     *
     */
    public GeoTransPoint() {
        super();
    }
    /**
     * @param x
     * @param y
     */
    public GeoTransPoint(double x, double y) {
        super();
        this.x = x;
        this.y = y;
        this.z = 0;
    }
    /**
     * @param x
     * @param y
     * @param y
     */
    public GeoTransPoint(double x, double y, double z) {
        super();
        this.x = x;
        this.y = y;
        this.z = 0;
    }
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
