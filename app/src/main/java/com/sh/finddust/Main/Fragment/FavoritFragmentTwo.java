package com.sh.finddust.Main.Fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sh.finddust.R;
import com.sh.finddust.Util.ClassificationValue;
import com.sh.finddust.Util.FaceIcon;
import com.sh.finddust.Util.ShareUtils;
import com.sh.finddust.Util.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sh.finddust.Main.MainActivity.MyFavorit;
import static com.sh.finddust.Main.MainActivity.MyFavoritData;
import static com.sh.finddust.Main.MainActivity.MyFavoritStation;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavoritFragmentTwo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FavoritFragmentTwo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoritFragmentTwo extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // 페이지명
    private static final int PAGE = 1;

    // 객체
    private TextView pm10, pm25, o3, so2, coValue, no2Value, title_message, station_name;
    private TextView toolbar_sido, toolbar_date;
    private ImageView face_icon, Pollutant_Detail;
    private ImageView share_button_two;

    // Grade/Value 토글
    private boolean pollutant = false;

    // 유틸
    private ClassificationValue grade;
    private Utils utils;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FavoritFragmentTwo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavoritFragmentTwo.
     */
    // TODO: Rename and change types and number of parameters
    public static FavoritFragmentTwo newInstance(String param1, String param2) {
        FavoritFragmentTwo fragment = new FavoritFragmentTwo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // 생성자
        grade = new ClassificationValue();
        utils = new Utils();
        FaceIcon faceIcon = new FaceIcon();

        // 객체 연결
        pm10 = (TextView) getActivity().findViewById(R.id.pm10Value_two);
        pm25 = (TextView) getActivity().findViewById(R.id.pm25Value_two);
        so2 = (TextView) getActivity().findViewById(R.id.so2Value_two);
        o3 = (TextView) getActivity().findViewById(R.id.o3Value_two);
        coValue = (TextView) getActivity().findViewById(R.id.coValue_two);
        no2Value = (TextView) getActivity().findViewById(R.id.no2Value_two);
        toolbar_sido = (TextView) getActivity().findViewById(R.id.sido_two);
        toolbar_date = (TextView) getActivity().findViewById(R.id.date_two);
        face_icon = (ImageView) getActivity().findViewById(R.id.face_icon_two);
        title_message = (TextView) getActivity().findViewById(R.id.title_message_two);
        Pollutant_Detail = (ImageView) getActivity().findViewById(R.id.Pollutant_Detail_two);
        station_name = (TextView) getActivity().findViewById(R.id.station_name_two);
        share_button_two = (ImageView) getActivity().findViewById(R.id.share_button_two);

        // 값 입력
        pm10.setText(grade.classficationPM10(MyFavoritData.get(PAGE).getPm10Value()));
        pm25.setText(grade.classficationPM25(MyFavoritData.get(PAGE).getPm25Value()));
        so2.setText(grade.classficationSo2(MyFavoritData.get(PAGE).getSo2Grade()));
        o3.setText(grade.classficationO3(MyFavoritData.get(PAGE).getO3Value()));
        coValue.setText(grade.classficationCo(MyFavoritData.get(PAGE).getCoValue()));
        no2Value.setText(grade.classficationNo2(MyFavoritData.get(PAGE).getNo2Value()));

        // 아이콘
        face_icon.setImageDrawable(getActivity().getResources().getDrawable(
                faceIcon.getIcon(grade.classficationPM10(MyFavoritData.get(PAGE).getPm10Value()))
        ));

        // 타이틀 메시지
        title_message.setText(grade.classficationPM10(MyFavoritData.get(PAGE).getPm10Value()));


        // 툴바(주소, 시간)
        toolbar_sido.setText(MyFavorit.get(PAGE).getSido().replace("null",""));
        toolbar_date.setText(getDate());

        // 상세보기 버튼
        Pollutant_Detail.setOnClickListener(this);

        // 측정소 버튼
        station_name.setText(MyFavoritStation.get(PAGE).getStationName());
        station_name.setOnClickListener(this);

        // 값 상세보기 버튼
        pm10.setOnClickListener(this);
        pm25.setOnClickListener(this);
        so2.setOnClickListener(this);
        o3.setOnClickListener(this);
        coValue.setOnClickListener(this);
        no2Value.setOnClickListener(this);

        // 공유하기 버튼
        share_button_two.setOnClickListener(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorit_fragment_two, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Pollutant_Detail_two:
                if (!pollutant) {
                    pollutant = true;
                    pm10.setText(MyFavoritData.get(PAGE).getPm10Value() + "㎍/㎥");
                    pm25.setText(MyFavoritData.get(PAGE).getPm25Value() + "㎍/㎥");
                    so2.setText(MyFavoritData.get(PAGE).getSo2Value() + "ppm");
                    o3.setText(MyFavoritData.get(PAGE).getO3Value() + "ppm");
                    coValue.setText(MyFavoritData.get(PAGE).getCoValue() + "ppm");
                    no2Value.setText(MyFavoritData.get(PAGE).getNo2Value() + "ppm");
                } else {
                    pollutant = false;
                    pm10.setText(grade.classficationPM10(MyFavoritData.get(PAGE).getPm10Value()));
                    pm25.setText(grade.classficationPM25(MyFavoritData.get(PAGE).getPm25Value()));
                    so2.setText(grade.classficationSo2(MyFavoritData.get(PAGE).getSo2Value()));
                    o3.setText(grade.classficationO3(MyFavoritData.get(PAGE).getO3Value()));
                    coValue.setText(grade.classficationCo(MyFavoritData.get(PAGE).getCoValue()));
                    no2Value.setText(grade.classficationNo2(MyFavoritData.get(PAGE).getNo2Value()));
                }
                break;
            case R.id.station_name_two:
                new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("측정소 정보")
                        .setCustomImage(R.drawable.ic_station)
                        .setContentText(MyFavoritStation.get(PAGE).getAddr())
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmText("확인")
                        .setConfirmClickListener(null)
                        .show();
                break;
            case R.id.pm10Value_two:
                getPolluteDialog("미세먼지");
                break;
            case R.id.pm25Value_two:
                getPolluteDialog("초미세먼지");
                break;
            case R.id.so2Value_two:
                getPolluteDialog("아황산가스");
                break;
            case R.id.o3Value_two:
                getPolluteDialog("오존");
                break;
            case R.id.coValue_two:
                getPolluteDialog("일산화탄소");
                break;
            case R.id.no2Value_two:
                getPolluteDialog("이산화질소");
                break;
            case R.id.share_button_two:
                ShareUtils share = new ShareUtils(this);
                String addr = "[" + MyFavorit.get(PAGE).getSido().replace("null","") + "]\n";
                String pm10 = "■ 미세먼지 : " + grade.classficationPM10(MyFavoritData.get(PAGE).getPm10Value()) + " (" + MyFavoritData.get(PAGE).getPm10Value() + "㎍/㎥)\n";
                String pm25 = "■ 초미세먼지 : " + grade.classficationPM10(MyFavoritData.get(PAGE).getPm25Value()) + " (" + MyFavoritData.get(PAGE).getPm25Value() + "㎍/㎥)";
                share.shareKakao(addr + pm10 + pm25, grade.classficationPM10(MyFavoritData.get(PAGE).getPm10Value()));
                break;
        }
    }

    private void getPolluteDialog(String value){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(value)
                .setContentText(utils.getPollutant(value))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmText("확인")
                .setConfirmClickListener(null)
                .show();

    }

    private String getDate(){
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd a hh:mm");
        return sdf.format(date);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
