package com.sh.finddust.Main;

import android.app.Application;

import com.tsengvn.typekit.Typekit;

/**
 * Created by 김지민 on 2017-12-04.
 */

public class BaseActivity extends Application {

    // 폰트 설정

    @Override
    public void onCreate(){
        super.onCreate();

        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "KOMACON.ttf"))
                .addBold(Typekit.createFromAsset(this, "KOMACON.ttf"));

    }
}
