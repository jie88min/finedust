package com.sh.finddust.Main.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sh.finddust.R;
import com.sh.finddust.Util.FaceIcon;
import com.sh.finddust.Util.ClassificationValue;
import com.sh.finddust.Util.ShareUtils;
import com.sh.finddust.Util.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sh.finddust.Main.MainActivity.Now_Location;
import static com.sh.finddust.Main.MainActivity.NowLocationData;
import static com.sh.finddust.Main.MainActivity.NowLocationStation;
import static com.sh.finddust.Main.MainActivity.SupportStation_NOW;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NowLocationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NowLocationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NowLocationFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    // 객체
    private TextView pm10, pm25, o3, so2, coValue, no2Value, title_message, station_name;
    private TextView toolbar_sido, toolbar_date;
    private ImageView face_icon, Pollutant_Detail;
    private ImageView share_button_now;

    // Grade/Value 토글
    private boolean pollutant = false;

    // 유틸
    private ClassificationValue grade;
    private Utils utils;

    public NowLocationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NowLocationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NowLocationFragment newInstance(String param1, String param2) {
        NowLocationFragment fragment = new NowLocationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_now_location, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // 생성자
        grade = new ClassificationValue();
        utils = new Utils();
        FaceIcon faceIcon = new FaceIcon();

        // 객체 연결
        pm10 = (TextView) getActivity().findViewById(R.id.pm10Value) ;
        pm25 = (TextView) getActivity().findViewById(R.id.pm25Value) ;
        so2 = (TextView) getActivity().findViewById(R.id.so2Value) ;
        o3 = (TextView) getActivity().findViewById(R.id.o3Value) ;
        coValue = (TextView) getActivity().findViewById(R.id.coValue);
        no2Value = (TextView) getActivity().findViewById(R.id.no2Value);
        toolbar_sido = (TextView) getActivity().findViewById(R.id.now_sido);
        toolbar_date = (TextView) getActivity().findViewById(R.id.now_date);
        face_icon = (ImageView) getActivity().findViewById(R.id.face_icon);
        title_message = (TextView) getActivity().findViewById(R.id.title_message);
        Pollutant_Detail = (ImageView) getActivity().findViewById(R.id.Pollutant_Detail);
        station_name = (TextView) getActivity().findViewById(R.id.station_name);
        share_button_now = (ImageView) getActivity().findViewById(R.id.share_button_now);

        // 값 입력
        pm10.setText(grade.classficationPM10(NowLocationData.get(SupportStation_NOW).getPm10Value()));
        pm25.setText(grade.classficationPM25(NowLocationData.get(SupportStation_NOW).getPm25Value()));
        so2.setText(grade.classficationSo2(NowLocationData.get(SupportStation_NOW).getSo2Grade()));
        o3.setText(grade.classficationO3(NowLocationData.get(SupportStation_NOW).getO3Grade()));
        coValue.setText(grade.classficationCo(NowLocationData.get(SupportStation_NOW).getCoValue()));
        no2Value.setText(grade.classficationNo2(NowLocationData.get(SupportStation_NOW).getNo2Value()));

        // 아이콘
        face_icon.setImageDrawable(getActivity().getResources().getDrawable(
                faceIcon.getIcon(grade.classficationPM10(NowLocationData.get(SupportStation_NOW).getPm10Value()))
                ));

        // 타이틀 메시지
        title_message.setText(grade.classficationPM10(NowLocationData.get(SupportStation_NOW).getPm10Value()));

        // 툴바(주소, 시간)
        Log.d("sidoNow", NowLocationStation.get(SupportStation_NOW).getStationName());
        toolbar_sido.setText(utils.EncordAddress(getActivity(), Now_Location.getLatitude(), Now_Location.getLongitude()).replace("null",""));
        toolbar_date.setText(getDate());

        // 상세보기 버튼
        Pollutant_Detail.setOnClickListener(this);

        // 측정소 버튼
        station_name.setText(NowLocationStation.get(SupportStation_NOW).getStationName());
        station_name.setOnClickListener(this);

        // 값 상세보기 버튼
        pm10.setOnClickListener(this);
        pm25.setOnClickListener(this);
        so2.setOnClickListener(this);
        o3.setOnClickListener(this);
        coValue.setOnClickListener(this);
        no2Value.setOnClickListener(this);

        // 공유하기 버튼
        share_button_now.setOnClickListener(this);
    }
  
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Pollutant_Detail:
                if (!pollutant) {
                    pollutant = true;
                    pm10.setText(NowLocationData.get(SupportStation_NOW).getPm10Value() + "㎍/㎥");
                    pm25.setText(NowLocationData.get(SupportStation_NOW).getPm25Value() + "㎍/㎥");
                    so2.setText(NowLocationData.get(SupportStation_NOW).getSo2Value() + "ppm");
                    o3.setText(NowLocationData.get(SupportStation_NOW).getO3Value() + "ppm");
                    coValue.setText(NowLocationData.get(SupportStation_NOW).getCoValue() + "ppm");
                    no2Value.setText(NowLocationData.get(SupportStation_NOW).getNo2Value() + "ppm");
                } else {
                    pollutant = false;
                    pm10.setText(grade.classficationPM10(NowLocationData.get(SupportStation_NOW).getPm10Value()));
                    pm25.setText(grade.classficationPM25(NowLocationData.get(SupportStation_NOW).getPm25Value()));
                    so2.setText(grade.classficationSo2(NowLocationData.get(SupportStation_NOW).getSo2Value()));
                    o3.setText(grade.classficationO3(NowLocationData.get(SupportStation_NOW).getO3Value()));
                    coValue.setText(grade.classficationCo(NowLocationData.get(SupportStation_NOW).getCoValue()));
                    no2Value.setText(grade.classficationNo2(NowLocationData.get(SupportStation_NOW).getNo2Value()));
                }
                break;
            case R.id.station_name:
                new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("측정소 정보")
                        .setCustomImage(R.drawable.ic_station)
                        .setContentText(NowLocationStation.get(SupportStation_NOW).getAddr())
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmText("확인")
                        .setConfirmClickListener(null)
                        .show();
                break;
            case R.id.now_sido:
                new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                        .setTitleText("현재위치")
                        .setCustomImage(R.drawable.ic_location)
                        .setContentText(NowLocationStation.get(SupportStation_NOW).getAddr())
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmText("확인")
                        .setConfirmClickListener(null)
                        .show();
                break;
            case R.id.pm10Value:
                getPolluteDialog("미세먼지");
                break;
            case R.id.pm25Value:
                getPolluteDialog("초미세먼지");
                break;
            case R.id.so2Value:
                getPolluteDialog("아황산가스");
                break;
            case R.id.o3Value:
                getPolluteDialog("오존");
                break;
            case R.id.coValue:
                getPolluteDialog("일산화탄소");
                break;
            case R.id.no2Value:
                getPolluteDialog("이산화질소");
                break;
            case R.id.share_button_now:
                ShareUtils share = new ShareUtils(this);
                String addr = "[" + utils.EncordAddress(getActivity(), Now_Location.getLatitude(), Now_Location.getLongitude()).replace("null","") + "]\n";
                String pm10 = "■ 미세먼지 : " + grade.classficationPM10(NowLocationData.get(SupportStation_NOW).getPm10Value()) + " (" + NowLocationData.get(SupportStation_NOW).getPm10Value() + "㎍/㎥)\n";
                String pm25 = "■ 초미세먼지 : " + grade.classficationPM10(NowLocationData.get(SupportStation_NOW).getPm25Value()) + " (" + NowLocationData.get(SupportStation_NOW).getPm25Value() + "㎍/㎥)";
                share.shareKakao(addr + pm10 + pm25, grade.classficationPM10(NowLocationData.get(SupportStation_NOW).getPm10Value()));
                break;
        }
    }

    private void getPolluteDialog(String value){
        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(value)
                .setContentText(utils.getPollutant(value))
                .showCancelButton(false)
                .setCancelClickListener(null)
                .setConfirmText("확인")
                .setConfirmClickListener(null)
                .show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private String getDate(){
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd a hh:mm");
        return sdf.format(date);
    }
}
