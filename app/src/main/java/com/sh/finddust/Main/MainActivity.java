package com.sh.finddust.Main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.sh.finddust.DTO.ApiNearStation;
import com.sh.finddust.DTO.ApiNowDustData;
import com.sh.finddust.DTO.LocationData;
import com.sh.finddust.Main.Fragment.MainFragment;
import com.sh.finddust.Nevigation.Favorit.FavoritFragment;
import com.sh.finddust.Nevigation.LocationSearch.LocationSearchFragment;
import com.sh.finddust.Nevigation.Setting.SettingFragment;
import com.sh.finddust.R;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /* Navigation */
    public int HOME = 0;
    public int FAVORIT = 1;
    public int LOCATION = 2;
    public int SETTING = 3;

    public ImageView[] Navigation;     // 0 : Home  1 : Favorit  2 : Location  3 : Setting
    public static TextView[] Toolbars;    // 0 : Address  2 : Date
    public ImageView share_button;

    // 즐겨찾기 Data
    public static ArrayList<LocationData> MyFavorit;
    public static ArrayList<ApiNowDustData> MyFavoritData;
    public static ArrayList<ApiNearStation> MyFavoritStation;
    public static ArrayList<Integer> SupportStation_FAV;

    // 현재 GPS Data
    public static ArrayList<ApiNowDustData> NowLocationData;
    public static ArrayList<ApiNearStation> NowLocationStation;
    public static int SupportStation_NOW = 0;
    public static Location Now_Location;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 배경 그라데이션
        LinearLayout layout = (LinearLayout) findViewById(R.id.background);
        AnimationDrawable animationDrawable = (AnimationDrawable) layout.getBackground();
        animationDrawable.setEnterFadeDuration(2000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        // 툴바
        setToolBar();

        // 네비게이션바
        setNavigation();

        // Splash -> Main 값 인텐트
        Intent intent = getIntent();
        MyFavorit = (ArrayList<LocationData>) intent.getSerializableExtra("MyFavorit");
        NowLocationData = (ArrayList<ApiNowDustData>) intent.getSerializableExtra("NowLocationData");
        NowLocationStation = (ArrayList<ApiNearStation>) intent.getSerializableExtra("NowLocationStation");
        MyFavoritData = (ArrayList<ApiNowDustData>) intent.getSerializableExtra("MyFavoritData");
        MyFavoritStation = (ArrayList<ApiNearStation>) intent.getSerializableExtra("MyFavoritStation");
        MyFavorit = (ArrayList<LocationData>) intent.getSerializableExtra("MyFavorit");
        SupportStation_FAV = intent.getIntegerArrayListExtra("SupportStation_FAV");
        SupportStation_NOW = intent.getIntExtra("SupportStation_NOW", 0);
        Now_Location = new Location("Mylocation");
        Now_Location.setLatitude(intent.getDoubleExtra("Latitude", 0));
        Now_Location.setLongitude(intent.getDoubleExtra("Longtitude", 0));

        // 프래그먼트 연결
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, new MainFragment());
        fragmentTransaction.commitAllowingStateLoss();

        // AdMob
        setAd();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    private void setNavigation() {
        Navigation = new ImageView[4];
        int[] btId = {R.id.HomeButton, R.id.FavoritButton, R.id.LocationButton, R.id.SettingButton};
        for (int i = 0; i < Navigation.length; i++) {
            Navigation[i] = (ImageView) findViewById(btId[i]);
            Navigation[i].setOnClickListener(this);
        }
        Navigation[0].setImageDrawable(getResources().getDrawable(R.drawable.ic_home_press));
    }

    private void setNavigationSelector(ImageView menu) {
        switch (menu.getId()) {
            case R.id.HomeButton:
                Navigation[HOME].setImageDrawable(getResources().getDrawable(R.drawable.ic_home_press));
                Navigation[FAVORIT].setImageDrawable(getResources().getDrawable(R.drawable.selector_favorit_button));
                Navigation[LOCATION].setImageDrawable(getResources().getDrawable(R.drawable.selector_add_location));
                Navigation[SETTING].setImageDrawable(getResources().getDrawable(R.drawable.selector_setting_buttn));
                break;
            case R.id.FavoritButton:
                Navigation[HOME].setImageDrawable(getResources().getDrawable(R.drawable.selector_home_buttn));
                Navigation[FAVORIT].setImageDrawable(getResources().getDrawable(R.drawable.ic_star_press));
                Navigation[LOCATION].setImageDrawable(getResources().getDrawable(R.drawable.selector_add_location));
                Navigation[SETTING].setImageDrawable(getResources().getDrawable(R.drawable.selector_setting_buttn));
                break;
            case R.id.LocationButton:
                Navigation[HOME].setImageDrawable(getResources().getDrawable(R.drawable.selector_home_buttn));
                Navigation[FAVORIT].setImageDrawable(getResources().getDrawable(R.drawable.selector_favorit_button));
                Navigation[LOCATION].setImageDrawable(getResources().getDrawable(R.drawable.ic_map_press));
                Navigation[SETTING].setImageDrawable(getResources().getDrawable(R.drawable.selector_setting_buttn));
                break;
            case R.id.SettingButton:
                Navigation[HOME].setImageDrawable(getResources().getDrawable(R.drawable.selector_home_buttn));
                Navigation[FAVORIT].setImageDrawable(getResources().getDrawable(R.drawable.selector_favorit_button));
                Navigation[LOCATION].setImageDrawable(getResources().getDrawable(R.drawable.selector_add_location));
                Navigation[SETTING].setImageDrawable(getResources().getDrawable(R.drawable.ic_settings_press));
                break;
            default:
                break;
        }
    }

    private void setToolBar() {
        share_button = (ImageView) findViewById(R.id.share_button);
        share_button.setOnClickListener(this);

        Toolbars = new TextView[2];
        int[] tbId = {R.id.toolbar_sido, R.id.toolbar_date};
        for (int i = 0; i < Toolbars.length; i++) {
            Toolbars[i] = (TextView) findViewById(tbId[i]);
        }
        Toolbars[0].setOnClickListener(this);
    }

    private void Load_Page(int page) {
        switch (page) {
            case 0: // 홈
                FragmentManager homefragment = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = homefragment.beginTransaction();
                fragmentTransaction.replace(R.id.container, new MainFragment());
                fragmentTransaction.commit();
                break;
            case 1: // 즐겨찾기
                FragmentManager favoritfragment = getSupportFragmentManager();
                FragmentTransaction favoritTransaction = favoritfragment.beginTransaction();
                favoritTransaction.replace(R.id.container, new FavoritFragment());
                favoritTransaction.commit();
                break;
            case 2: // 주소검색
                FragmentManager locationfragment = getSupportFragmentManager();
                FragmentTransaction locationTransaction = locationfragment.beginTransaction();
                locationTransaction.replace(R.id.container, new LocationSearchFragment());
                locationTransaction.commit();
                break;
            case 3: // 설정
                FragmentManager settingfragment = getSupportFragmentManager();
                FragmentTransaction settingTransaction = settingfragment.beginTransaction();
                settingTransaction.replace(R.id.container, new SettingFragment());
                settingTransaction.commit();
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.HomeButton:
                setNavigationSelector(Navigation[0]);
                Load_Page(HOME);
                share_button.setVisibility(View.VISIBLE);
                break;
            case R.id.FavoritButton:
                setNavigationSelector(Navigation[1]);
                Load_Page(FAVORIT);
                share_button.setVisibility(View.GONE);
                break;
            case R.id.LocationButton:
                setNavigationSelector(Navigation[2]);
                Load_Page(LOCATION);
                share_button.setVisibility(View.GONE);
                break;
            case R.id.SettingButton:
                setNavigationSelector(Navigation[3]);
                Load_Page(SETTING);
                share_button.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    private void setAd(){
        final InterstitialAd mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.admob_screen));

        // 광고는 하루에 한번
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        String adwatch = prefs.getString("AD_Watch", "");

        if (!getDate().equals(adwatch)) {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                        editor.putString("AD_Watch", getDate());
                        editor.apply();
                    }
                }
            });
        }
    }

    private String getDate(){
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    private final long FINISH_INTERVAL_TIME = 2000;
    private long backPressedTime = 0;
    @Override
    public void onBackPressed() {
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;

        if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime) {
            super.onBackPressed();
        } else {
            backPressedTime = tempTime;
            Toast.makeText(this, "'뒤로'를 한 번 더 눌러 종료합니다.", Toast.LENGTH_SHORT).show();
        }
    }
}
