package com.sh.finddust.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sh.finddust.DTO.LocationData;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by 김지민 on 2017-12-08.
 */

public class FavoritManager {
    private Context context;
    private String key;
    private ArrayList<LocationData> values;

    private final int FAVORIT_NUM = 3;

    public FavoritManager(Context context, String key, ArrayList<LocationData> values){
        this.context = context;
        this.key = key;
        this.values = values;
    }

    // 즐겨찾기 저장
    public void setFavorit() {
        if (values != null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = prefs.edit();

            Gson gson = new Gson();

            Type listType = new TypeToken<ArrayList<LocationData>>() {}.getType();
            String json = gson.toJson(values, listType);

            if (!json.equals("")) {
                editor.putString(key, json);
            } else {
                editor.putString(key, null);
            }
            editor.apply();

            Log.d("SAVE", prefs.getString(key, null));
        }
    }

    // 즐겨찾기 불러오기
    public ArrayList<LocationData> getFavorit(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        ArrayList<LocationData> locationDatas = new ArrayList<LocationData>();
        String json = prefs.getString(key, "");

        Gson gson = new Gson();

        if (!json.equals("")) {
            Type type = new TypeToken<ArrayList<LocationData>>(){}.getType();
            ArrayList<LocationData> location = gson.fromJson(json, type);
            locationDatas.addAll(location);
        }
        Log.d("LOAD", locationDatas.toString());
        return locationDatas;
    }

    // 즐겨찾기 체크
    public int CheakFavorit(String location){
        int result_code = 0;

        for (int i = 0; i < values.size(); i++) {
            if (values.get(i).getAdress().equals(location)) result_code = 1;
        }

        if (values.size() >= FAVORIT_NUM) {
            result_code = 2;
        }

        return result_code;
    }

    // 즐겨찾기 총 갯수
    public int CountFavorit(){
        return values.size();
    }

    // 즐겨찾기 추가 가능 갯수
    public int getFavoritSize(){
        return FAVORIT_NUM;
    }
}
