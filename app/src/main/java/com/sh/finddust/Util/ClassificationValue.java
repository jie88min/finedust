package com.sh.finddust.Util;

/**
 * Created by 이창훈 on 2017-12-10.
 */

public class ClassificationValue {

    // 미세먼지 분류
    public String classficationPM10(String value){
        int pm10;

        if (value == null || value.equals("")) {
            return "-";
        } else if(!value.equals("-")){
            pm10 = Integer.parseInt(value);
        } else {
            return "-";
        }

        if(pm10 <= 30){
            return "좋음";
        } else if(31 <= pm10 && pm10 <= 50){
            return "보통";
        } else if(51 <= pm10 && pm10 <= 100){
            return "나쁨";
        } else if(101 <= pm10){
            return "심각";
        }

        return null;
    }

    // 초미세먼지 분류
    public String classficationPM25(String value){
        int pm25;

        if (value == null || value.equals("")) {
            return "-";
        } else if(!value.equals("-")){
            pm25 = Integer.parseInt(value);
        } else {
            return "-";
        }

        if(pm25 <= 15){
            return "좋음";
        } else if(16 <= pm25 && pm25 <= 25){
            return "보통";
        } else if(26 <= pm25 && pm25 <= 50){
            return "나쁨";
        } else if(51 <= pm25){
            return "심각";
        }

        return null;
    }

    // 아황산가스 수치 분류
    public String classficationSo2(String value){

        double so2;
        if (value == null || value.equals("")) {
            return "-";
        } else if(!value.equals("-")){
            so2 = Double.parseDouble(value);
        } else {
            return "-";
        }

        if(so2 <= 0.02){
            return "좋음";
        } else if(so2 >= 0.021 && so2 <= 0.05){
            return "보통";
        } else if(so2 >= 0.051 && so2 <= 0.15){
            return "나쁨";
        } else if(so2 > 0.15){
            return "심각";
        } else {
            return "-";
        }
    }

    // 이산화질소 수치 분류
    public String classficationNo2(String value){
        double no2;

        if (value == null || value.equals("")) {
            return "-";
        } else if(!value.equals("-")){
            no2 = Double.parseDouble(value);
        } else {
            return "-";
        }

        if(no2 <= 0.03){
            return "좋음";
        } else if(no2 >= 0.031 && no2 <= 0.06){
            return "보통";
        } else if(no2 >= 0.061 && no2 <= 0.2){
            return "나쁨";
        } else if(no2 > 0.2){
            return "심각";
        } else {
            return "-";
        }
    }

    // 일산화탄소 수치 분류
    public String classficationCo(String value){
        double co;

        if (value == null || value.equals("")) {
            return "-";
        } else if(!value.equals("-")){
            co = Double.parseDouble(value);
        } else {
            return "-";
        }

        if(co <= 2){
            return "좋음";
        } else if(co >= 2.01 && co <= 9){
            return "보통";
        } else if(co >= 9.01 && co <= 15){
            return "나쁨";
        } else if(co > 15){
            return "심각";
        } else {
            return "-";
        }
    }

    // 오존 수치 분류
    public String classficationO3(String value){
        double o3;

        if (value == null || value.equals("")) {
            return "-";
        } else if(!value.equals("-")){
            o3 = Double.parseDouble(value);
        } else {
            return "-";
        }

        if(o3 <= 0.03){
            return "좋음";
        } else if(o3 >= 0.031 && o3 <= 0.09){
            return "보통";
        } else if(o3 >= 0.091 && o3 <= 0.15){
            return "나쁨";
        } else if(o3 > 0.15){
            return "심각";
        } else {
            return "-";
        }
    }

    // 통합 대기 지수 수치 분류
    public String classficationKhai(String value){
        double khai;

        if (value == null || value.equals("")) {
            return "-";
        } else if(!value.equals("-")){
            khai = Double.parseDouble(value);
        } else {
            return "-";
        }

        if(khai <= 50){
            return "좋음";
        } else if(khai >= 51 && khai <= 100){
            return "보통";
        } else if(khai >= 101 && khai <= 250){
            return "나쁨";
        } else if(khai > 251){
            return "심각";
        } else {
            return "-";
        }
    }
}
