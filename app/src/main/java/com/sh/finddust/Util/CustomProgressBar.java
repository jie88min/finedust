package com.sh.finddust.Util;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;

import com.sh.finddust.R;


/**
 * Created by Junho on 2017-03-03.
 */

public class CustomProgressBar extends Dialog {
    public CustomProgressBar(Activity activity) {
        super(activity);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //다이얼 로그 제목을 삭제하자
        setContentView(R.layout.custom_progress_bar); // 다이얼로그 보여줄 레이아웃

    }
}