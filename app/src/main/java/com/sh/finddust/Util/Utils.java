package com.sh.finddust.Util;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;

/**
 * Created by 김지민 on 2017-12-04.
 */

public class Utils {
    private final String pm10 = "미세먼지";
    private final String pm25 = "초미세먼지";
    private final String o3 = "오존";
    private final String so2 = "아황산가스";
    private final String co = "일산화탄소";
    private final String no2 = "이산화질소";

    public String EncordAddress(Activity activitys, double lat, double lng) {
        String addr = "";
        List<Address> list;
        Geocoder geocoder = new Geocoder(activitys);
        try {
            list = geocoder.getFromLocation(lat, lng, 1);
            if (list != null) {
                Address a = list.get(0);
                addr = a.getLocality() + " " + a.getThoroughfare();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return addr;
    }

    public String getPollutant(String value) {
        String result = "";
        switch (value) {
            case pm10:
                result = "미세먼지는 천식과 같은 호흡기계 질병을 악화시키고, 폐 기능의 저하를 초래한다. 초미세먼지는 입자가 미세하여 코 점막을 통해 걸러지지 않고 흡입시 폐포까지 직접 침투하여 천식이나 폐질환의 유병률과 조기사망률을 증가시킨다. 또한 미세먼지는 시정을 악화시키고, 식물의 잎 표면에 침적되어 신진대사를 방해하며, 건출물이나 유적물 및 동상 등에 퇴적되어 부식을 일으킨다.";
                break;
            case pm25:
                result = "미세먼지는 천식과 같은 호흡기계 질병을 악화시키고, 폐 기능의 저하를 초래한다. 초미세먼지는 입자가 미세하여 코 점막을 통해 걸러지지 않고 흡입시 폐포까지 직접 침투하여 천식이나 폐질환의 유병률과 조기사망률을 증가시킨다. 또한 미세먼지는 시정을 악화시키고, 식물의 잎 표면에 침적되어 신진대사를 방해하며, 건출물이나 유적물 및 동상 등에 퇴적되어 부식을 일으킨다.";
                break;
            case o3:
                result = "오존에 반복 노출시에는 폐에 피해를 줄 수 있는데, 가슴의 통증, 기침, 메스꺼움, 목 자극, 소화 등에 영향을 미치며, 기관지염, 심장질환, 폐기종 및 천식을 악화시키고, 폐활량을 감소 시킬 수 있다. 특히 기관지 천식환자나 호흡기 질환자, 어린이, 노약자 등에게는 많은 영향을 미치므로 주의해야 할 필요가 있다. 또한 농작물과 식물에 직접적으로 영향을 미쳐 수확량이 감소되기도 하며 잎이 말라 죽기도 한다.";
                break;
            case so2:
                result = "고농도의 아황산가스는 옥외 활동이 많고 천식에 걸린 어른과 어린이에게 일시적으로 호흡장애를 일으킬 수 있으며, 고농도에 폭로될 경우 호흡기계 질환을 일으키고 심장혈관 질환을 악화시키는 것으로 알려져 있다. 질소산화물과 함께 산성비의 주요원인 물질로 토양등의 산성화에 영향을 미치고 바람에 의해 장거리 수송되어 다른 지역에 영향을 주며 식물의 잎맥손상 등을 일으키고 시정장애를 일으키며 각종 구조물의 부식을 촉진시킨다.";
                break;
            case co:
                result = "일산화탄소의 인체 영향을 살펴보면, 혈액순환 중에서 산소운반 역할을 하는 헤모그로빈을 카르복실헤모글로빈(COHb)으로 변성시켜 산소의 운반기능을 저하시키며, 고농도의 일산화탄소는 유독성이 있어 건강한 사람에게도 치명적인 해를 입힌다.";
                break;
            case no2:
                result = "질소산화물의 인체영향을 살펴보면, 일산화질소(NO) 보다는 이산화질소(NO2) 가 인체에 더욱 큰 피해를 주는 것으로 알려져 있다. 고농도에 노출되면 눈, 코 등의 점막에서 만성 기관지염, 폐렴, 폐출혈, 폐수종의 발병으로까지 발전할 수 있는 것으로 보고되고 있으며, 식물에 대한 피해로는 식물세포를 파괴하여 꽃식물의 잎에 갈색이나 흑갈색의 반점이 생기게 한다.";
                break;
        }
        return result;
    }
}
