package com.sh.finddust.Util;

import android.util.Log;

import com.sh.finddust.DTO.ApiNearStation;
import com.sh.finddust.DTO.ApiNowDustData;
import com.sh.finddust.DTO.LocationData;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static com.sh.finddust.Splash.SplashActivity.SupportStation_NOW;
import static com.sh.finddust.Splash.SplashActivity.SupportStation_FAV;
import static com.sh.finddust.Widget.AppWidget.SupportStation_WIDGET;

/**
 * Created by 이창훈 on 2017-12-04.
 */

public class GetAPIData {
    //근접 측정소 목록 조회 api url
    private final String apiGetNearByStation = "http://openapi.airkorea.or.kr/openapi/services/rest/MsrstnInfoInqireSvc/getNearbyMsrstnList?pageNo=1&numOfRows=10";
    //측정소별 실시간 데이터 api url
    private final String apiRealTimeDust = "http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty?&dataTerm=DAILY&pageNo=1&numOfRows=10";
    //API key
    private String apiKey = Global.API_KEY;
    //API version
    private final String version = "&ver=1.3";
    //API return Type
    private final String type = "&_returnType=json";


    private String stationName ="&stationName="; // url 요청 측정소 명
    private ArrayList<ApiNearStation> station;   // 측정소 명
    private String strTmX = "&tmX=";             // url 요청 tmX
    private String strTmY = "&tmY=";             // url 요청 tmY

    private ApiNearStation FavoritNearStation;   // 즐겨찾기 측정소

    private double tmX;     // tmX 좌표
    private double tmY;     // tmY 좌표

    public GetAPIData(ArrayList<ApiNearStation> station){              // 측정소명 받는 생성자
        this.station = station;
    }

    public GetAPIData(double tmx, double tmy){      // 좌표값 받는 생성자
        Log.d("error", String.valueOf(tmx));
        this.tmX = tmx;
        this.tmY = tmy;
        strTmX = strTmX + tmX;
        strTmY = strTmY + tmY;
    }

    // 측정소 실시간 미세먼지 정보 API
    public ArrayList<ApiNowDustData> getNowDustAPI() throws IOException {
        StringBuffer jsonHtml;
        ArrayList<ApiNowDustData> dustData = null;
        InputStream uis = null;
        BufferedReader br = null;

        try {
            for (int i = 0; i < station.size(); i++){
                URL u = new URL(apiRealTimeDust + stationName + URLEncoder.encode(station.get(i).getStationName(), "UTF-8")+ apiKey + version + type);
                uis = u.openStream();
                Log.e("URL", u.toString());
                br = new BufferedReader(new InputStreamReader(uis, "UTF-8"));

                jsonHtml = new StringBuffer();
                String line;

                while ((line = br.readLine()) != null) {
                    jsonHtml.append(line + "\r\n");
                }

                ParseJSON parse = new ParseJSON();
                dustData = parse.parseNowDustData(jsonHtml);
                Log.d("NowDust", "[" + i + "]" + dustData.toString());
                if (!dustData.isEmpty()){
                    if (!dustData.get(i).getPm10Value().isEmpty() && !dustData.get(i).getPm10Value().equals("") && !dustData.get(i).getPm10Value().equals("-")) {
                        SupportStation_NOW = i;
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(br != null)
                br.close();
            if(uis != null)
                uis.close();
        }

        return dustData;
    }

    // 즐겨찾기 실시간 미세먼지 정보 API
    public ApiNowDustData addFavoritDustAPI(int favorit) throws IOException {
        ArrayList<ApiNowDustData> dustData;
        ApiNowDustData supportData = null;
        InputStream uis = null;
        BufferedReader br = null;
        StringBuffer jsonHtml;
        try {
            for (int i = 0; i < station.size(); i++){

                URL u = new URL(apiRealTimeDust + stationName + URLEncoder.encode(station.get(i).getStationName(), "UTF-8")+ apiKey + version + type);
                uis = u.openStream();
                br = new BufferedReader(new InputStreamReader(uis, "UTF-8"));

                jsonHtml = new StringBuffer();
                String line;

                while ((line = br.readLine()) != null) {
                    jsonHtml.append(line + "\r\n");
                }

                ParseJSON parse = new ParseJSON();
                dustData = parse.parseNowDustData(jsonHtml);
                Log.d("Favorit" + favorit, "[" + i + "]" + dustData.toString());
                if (!dustData.isEmpty()) {
                    if (!dustData.get(i).getPm10Value().isEmpty() && !dustData.get(i).getPm10Value().equals("") && !dustData.get(i).getPm10Value().equals("-")){
                        supportData = dustData.get(i);
                        FavoritNearStation = new ApiNearStation(station.get(i).getStationName(), station.get(i).getAddr(), station.get(i).getTm());
                        SupportStation_FAV.add(i);
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(br != null)
                br.close();
            if(uis != null)
                uis.close();
        }

        return supportData;
    }

    // 즐겨찾기 측정소
    public ApiNearStation getFavoritNearStation(){
        return FavoritNearStation;
    }

    // 근접 측정소 목록 API
    public ArrayList<ApiNearStation> getNearStationAPI() throws IOException{
        StringBuffer jsonHtml = new StringBuffer();
        ArrayList<ApiNearStation> stationData = new ArrayList<ApiNearStation>();
        InputStream uis = null;
        BufferedReader br = null;

        try {
            URL u = new URL(apiGetNearByStation + strTmX + strTmY + apiKey + type);
            uis = u.openStream();
            br = new BufferedReader(new InputStreamReader(uis, "UTF-8"));

            String line = null;
            while ((line = br.readLine()) != null) {
                jsonHtml.append(line + "\r\n");
            }
            Log.e("getNearStationAPI", jsonHtml.toString());

            ParseJSON parse = new ParseJSON();
            stationData = parse.parseNearStation(jsonHtml);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(br != null)
                br.close();
            if(uis != null)
                uis.close();
        }

        return stationData;
    }
}
