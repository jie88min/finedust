package com.sh.finddust.Util;

import android.graphics.drawable.Drawable;

import com.sh.finddust.R;

/**
 * Created by 김지민 on 2017-12-10.
 */

public class FaceIcon {
    private final String GOOD = "좋음";
    private final String NORMAL = "보통";
    private final String BAD = "나쁨";
    private final String SERIOUS = "심각";

    private int[] good = {R.drawable.ic_good};
    private int[] normal = {R.drawable.ic_normal};
    private int[] bad = {R.drawable.ic_bad};
    private int[] serious = {R.drawable.ic_serious};

    public FaceIcon(){}

    // 상태 아이콘
    public int getIcon(String value) {
        int result = 0;
        switch (value) {
            case GOOD:
                result = good[0];
                break;
            case NORMAL:
                result = normal[0];
                break;
            case BAD:
                result = bad[0];
                break;
            case SERIOUS:
                result = serious[0];
                break;
            case "-":
                result = R.drawable.ic_close;
        }
        return result;
    }
}
