package com.sh.finddust.Util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.View;


import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
import com.kakao.util.KakaoParameterException;
import com.sh.finddust.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 김지민 on 2017-12-17.
 */

public class ShareUtils {
    private Fragment fragment;

    public ShareUtils(Fragment fragment) {
        this.fragment = fragment;
    }

    public void shareKakao(String message, String state){
        Intent startLink = fragment.getActivity().getPackageManager().getLaunchIntentForPackage("com.kakao.talk");

        if (startLink != null) {
            if (!message.equals("") && !state.equals("")) {
                try {
                    final KakaoLink kakaoLink = KakaoLink.getKakaoLink(fragment.getActivity().getApplicationContext());
                    final KakaoTalkLinkMessageBuilder kakaoBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
                    String url = "";

                    String good = "http://postfiles4.naver.net/MjAxNzEyMThfMTcw/MDAxNTEzNTg0Njg4NTk1.FwWVVRtlnt5f342Blca4E3nF0UzcdxuQ-WrEl-h4yvcg.p-3JX4eJqynsK6uLcB9ZopMhnVllc0muz6lhGBvkM2wg.PNG.jie88min/good.png?type=w1";
                    String normal = "http://postfiles6.naver.net/MjAxNzEyMThfNDkg/MDAxNTEzNTg0Njg4Njkx.zmez7MXre0FuX2-e8EpAdFPJedUbaTRIkEIdxl66oiAg.xlGHeEoQ-rVUNpjvmXyuUaEeu9cfylxVE3XcWF6feTkg.PNG.jie88min/normal.png?type=w1";
                    String bad = "http://postfiles9.naver.net/MjAxNzEyMThfNjgg/MDAxNTEzNTg0Njg4Nzg3.hCNMnhG4ibvqboctBB7zX6M39kp09JHqhP9ug7H7Mq8g.CJD1JCFdrbbZsjIQRXmYEUKxTFjdfDeWPvcWoJPuPosg.PNG.jie88min/bad.png?type=w1";
                    String serious = "http://postfiles9.naver.net/MjAxNzEyMThfMjgy/MDAxNTEzNTg0Njg4OTAz.Q_OGvwxGAmozDZa5QjdmssZWx6-CNTPg7DvqzbYpe0Ig._3fHDxqfGb0PuZBH78evRKfM8ant49XFkyYyiJ02b3cg.PNG.jie88min/serious.png?type=w1";

                    /* 메시지 추가 */
                    kakaoBuilder.addText(message);

                    /* 이미지 추가 */
                    switch (state) {
                        case "좋음":
                            url = good;
                            break;
                        case "보통":
                            url = normal;
                            break;
                        case "나쁨":
                            url = bad;
                            break;
                        case "심각":
                            url = serious;
                            break;
                    }
                    kakaoBuilder.addImage(url, 128, 128);

                    /* 버튼 */
                    kakaoBuilder.addAppButton("바로가기");

                    /* 전송 */
                    kakaoLink.sendMessage(kakaoBuilder, fragment.getActivity());
                }catch (KakaoParameterException e){
                    e.printStackTrace();
                }
            }
        } else {
            new SweetAlertDialog(fragment.getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("공유하기")
                    .setContentText("공유하기 기능을 이용하기 위해서는 '카카오톡'이 설치되어 있어야 합니다.")
                    .showCancelButton(false)
                    .setConfirmText("확인")
                    .setConfirmClickListener(null)
                    .show();
        }
    }

    public void getHashKey(Activity activity){
        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
