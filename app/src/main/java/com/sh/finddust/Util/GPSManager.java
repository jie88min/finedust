package com.sh.finddust.Util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.sh.finddust.DTO.ApiNearStation;
import com.sh.finddust.DTO.ApiNowDustData;
import com.sh.finddust.DTO.GeoTransPoint;
import com.sh.finddust.DTO.LocationData;
import com.sh.finddust.Main.MainActivity;
import com.sh.finddust.R;
import com.sh.finddust.Splash.SplashActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sh.finddust.Splash.SplashActivity.MyFavorit;
import static com.sh.finddust.Splash.SplashActivity.MyFavoritData;
import static com.sh.finddust.Splash.SplashActivity.MyFavoritStation;
import static com.sh.finddust.Splash.SplashActivity.SupportStation_FAV;
import static com.sh.finddust.Splash.SplashActivity.NowLocationData;
import static com.sh.finddust.Splash.SplashActivity.NowLocationStation;
import static com.sh.finddust.Splash.SplashActivity.SupportStation_NOW;


/**
 * Created by 김지민 on 2017-12-06.
 */

public class GPSManager implements ActivityCompat.OnRequestPermissionsResultCallback{

    public LocationManager locationManager;
    public boolean isGPSEnabled = false;
    public boolean isNetworkEnabled = false;
    public Location location;
    public Activity activitys;
    public Context context;

    private SweetAlertDialog dialog;

    public GPSManager(final Activity activity) {
        this.activitys = activity;
        this.context = activity.getApplicationContext();
        this.dialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.location = new Location("Mylocation");

        ProviderCheck();
    }

    // 프로바이더 체크
    public void ProviderCheck(){
        // GPS 프로바이더 사용가능여부
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        // 네트워크 프로바이더 사용가능여부
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    // 로케이션 리스너
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location locations) {
            location = locations;
            locationManager.removeUpdates(this);
            new Loading(locations).execute();
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    // GPS 로드
    public void getGPS(){
        if (ContextCompat.checkSelfPermission(activitys, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activitys, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            if (!NetworkConnection()) {
                dialog.setTitleText("네트워크 오류")
                        .setContentText("설정에서 네트워크 사용을 체크 해주세요.")
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmText("설정")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                activitys.startActivity(intent);
                                activitys.finish();
                            }
                        }).show();
            } else if (!isGPSEnabled && !isNetworkEnabled) {
                dialog.setTitleText("GPS 오류")
                        .setContentText("설정에서 위치정보 사용을 체크 해주세요.")
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmText("설정")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activitys.startActivity(intent);
                                activitys.finish();
                            }
                        })
                        .show();
            } else {
                try{
                    if (isNetworkEnabled) locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                    else if (isGPSEnabled) locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 네트워크 체크
    private boolean NetworkConnection(){
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isMobileConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        boolean isWifiAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
        boolean isWifiConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        if((isWifiAvailable && isWifiConnect) || (isMobileConnect)) {
            return true;
        } else {
            return false;
        }
    }

    // 퍼미션 결과 처리
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("LOG", "퍼미션 추가완료");
                    getGPS();
                } else {
                    activitys.finish();
                }
                break;
            }
        }
    }

    public class Loading extends AsyncTask<Void, Void, Void>
    {
        private Location mLocation;
        // 좌표
        private GeoTransPoint oGeo;
        private GeoTransPoint oTm;

        public Loading(Location location) {
            this.mLocation = location;
        }
        @Override
        protected void onPreExecute(){
        }

        @Override
        protected Void doInBackground(Void... voids) {
            NowLocationData = new ArrayList<ApiNowDustData>();
            NowLocationStation = new ArrayList<ApiNearStation>();

            if(mLocation != null) {
                oGeo = new GeoTransPoint(mLocation.getLongitude(), mLocation.getLatitude());
                oTm = GeoTrans.convert(GeoTrans.GEO, GeoTrans.TM, oGeo);
                Log.e("LOCATION", "tmX: " + oTm.getX() + ",  tmY : " + oTm.getY());
                try{
                    GetAPIData reqestStation = new GetAPIData(oTm.getX(), oTm.getY());
                    NowLocationStation = reqestStation.getNearStationAPI();

                    GetAPIData requestDust = new GetAPIData(NowLocationStation);
                    NowLocationData = requestDust.getNowDustAPI();

                    getFavorit(MyFavorit);

                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        public void onPostExecute(Void avoid){
            super.onPreExecute();
            Intent intent = new Intent(activitys, MainActivity.class);
            intent.putExtra("Longtitude", mLocation.getLongitude());
            intent.putExtra("Latitude", mLocation.getLatitude());
            intent.putExtra("MyFavorit", MyFavorit);
            intent.putExtra("NowLocationData", NowLocationData);
            intent.putExtra("NowLocationStation", NowLocationStation);
            intent.putExtra("MyFavoritData", MyFavoritData);
            intent.putExtra("MyFavoritStation", MyFavoritStation);
            intent.putExtra("SupportStation_FAV", SupportStation_FAV);
            intent.putExtra("SupportStation_NOW", SupportStation_NOW);
            activitys.startActivity(intent);
            activitys.finish();

        }
    }

    public void getFavorit(ArrayList<LocationData> MyFavorit){
        MyFavoritData = new ArrayList<ApiNowDustData>();
        MyFavoritStation = new ArrayList<ApiNearStation>();
        SupportStation_FAV = new ArrayList<Integer>();

        for(int i=0; i<MyFavorit.size();i++){
            try{
                GetAPIData reqestStation = new GetAPIData(Double.parseDouble(MyFavorit.get(i).getTmX()), Double.parseDouble(MyFavorit.get(i).getTmY()));

                GetAPIData requestDust = new GetAPIData(reqestStation.getNearStationAPI());

                MyFavoritData.add(requestDust.addFavoritDustAPI(i));
                MyFavoritStation.add(requestDust.getFavoritNearStation());
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
