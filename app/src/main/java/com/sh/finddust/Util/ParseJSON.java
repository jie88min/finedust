package com.sh.finddust.Util;

import android.util.Log;

import com.sh.finddust.DTO.ApiNearStation;
import com.sh.finddust.DTO.ApiNowDustData;
import com.sh.finddust.DTO.LocationData;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;


/**
 * Created by 이창훈 on 2017-12-04.
 */

public class ParseJSON {

    // 측정값 Parser
    public ArrayList<ApiNowDustData> parseNowDustData(StringBuffer jsonString) {
        ArrayList<ApiNowDustData> data = new ArrayList<ApiNowDustData>();

        JSONObject object = (JSONObject) JSONValue.parse(jsonString.toString());
        JSONArray bodyArray = (JSONArray) object.get("list");

        for(int i=0; i < bodyArray.size(); i++){
            JSONObject jsonData = (JSONObject) bodyArray.get(i);
            ApiNowDustData apiNowDustData = new ApiNowDustData();

            apiNowDustData.setDateTime(jsonData.get("dataTime").toString());
            apiNowDustData.setSo2Value(jsonData.get("so2Value").toString());
            apiNowDustData.setCoValue(jsonData.get("coValue").toString());
            apiNowDustData.setO3Value(jsonData.get("o3Value").toString());
            apiNowDustData.setNo2Value(jsonData.get("no2Value").toString());
            apiNowDustData.setPm10Value(jsonData.get("pm10Value").toString());
            apiNowDustData.setPm10Value24(jsonData.get("pm10Value24").toString());
            apiNowDustData.setPm25Value(jsonData.get("pm25Value").toString());
            apiNowDustData.setPm25Value24(jsonData.get("pm25Value24").toString());
            apiNowDustData.setKhaiValue(jsonData.get("khaiValue").toString());

            apiNowDustData.setKhaiGrade(jsonData.get("khaiGrade").toString());
            apiNowDustData.setSo2Grade(jsonData.get("so2Grade").toString());
            apiNowDustData.setCoGrade(jsonData.get("coGrade").toString());
            apiNowDustData.setO3Grade(jsonData.get("o3Grade").toString());
            apiNowDustData.setNo2Grade(jsonData.get("no2Grade").toString());
            apiNowDustData.setPm10Grade(jsonData.get("pm10Grade").toString());
            apiNowDustData.setPm25Grade(jsonData.get("pm25Grade").toString());
            apiNowDustData.setPm10Grade1h(jsonData.get("pm10Grade1h").toString());
            apiNowDustData.setPm25Grade1h(jsonData.get("pm25Grade1h").toString());

            data.add(apiNowDustData);

        }
        return data;
    }

    // 가까운 측정소 Parser
    public ArrayList<ApiNearStation> parseNearStation(StringBuffer jsonString) {
        ArrayList<ApiNearStation> data = new ArrayList<ApiNearStation>();

        JSONObject object = (JSONObject) JSONValue.parse(jsonString.toString());
        Log.d("parseNearStation_object", object.toString());
        JSONArray bodyArray = (JSONArray) object.get("list");
        Log.d("parseNearStation_body", bodyArray.toString());

        for(int i=0; i < bodyArray.size(); i++){
            JSONObject jsonData = (JSONObject)bodyArray.get(i);
            ApiNearStation apiNearStation = new ApiNearStation(jsonData.get("stationName").toString(),
                    jsonData.get("addr").toString(), Double.parseDouble(jsonData.get("tm").toString()));
            data.add(apiNearStation);
        }
        return data;
    }
}
