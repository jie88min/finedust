package com.sh.finddust.Splash;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.sh.finddust.BuildConfig;
import com.sh.finddust.DTO.ApiNearStation;
import com.sh.finddust.DTO.ApiNowDustData;
import com.sh.finddust.DTO.GeoTransPoint;
import com.sh.finddust.DTO.LocationData;
import com.sh.finddust.Main.MainActivity;
import com.sh.finddust.R;
import com.sh.finddust.Util.FavoritManager;
import com.sh.finddust.Util.GPSManager;
import com.sh.finddust.Util.GeoTrans;
import com.sh.finddust.Util.GetAPIData;
import com.sh.finddust.Util.Global;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class SplashActivity extends AppCompatActivity {

//    public GPSManager GPSmanager;
    private ImageView progress_icon;

    // SharedPreferences
    public static SharedPreferences data_saver;

    // 즐겨찾기 Data
    public static ArrayList<LocationData> MyFavorit;
    public static ArrayList<ApiNowDustData> MyFavoritData;
    public static ArrayList<ApiNearStation> MyFavoritStation;
    public static ArrayList<Integer> SupportStation_FAV;

    // 현재 GPS Data
    public static ArrayList<ApiNowDustData> NowLocationData;
    public static ArrayList<ApiNearStation> NowLocationStation;
    public static int SupportStation_NOW = 0;

    // 즐겨찾기 관리
    public FavoritManager favoritManager;

    // 대기시간 0.5초
    private final int SPLASH_DISPLAY_LENGTH = 500;

    private FirebaseRemoteConfig remoteConfig;

    // Store Vertion
    public static String storeVersion = "";

    // Device Vertion
    public static String deviceVersion = "";

    public static String MARKET_URL = "com.sh.finddust";

    FusedLocationProviderClient mFusedLocationClient;
    LocationRequest mLocationRequest;
    LocationManager locationManager;
    Location mLocation;

    // 좌표
    private GeoTransPoint oGeo;
    private GeoTransPoint oTm;

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseRemoteConfigSettings remoteConfigSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(false)
                .build();
        remoteConfig = FirebaseRemoteConfig.getInstance();
        remoteConfig.setConfigSettings(remoteConfigSettings);
        remoteConfig.setDefaults(R.xml.remote_config_defaults);

        if (NetworkConnection(this)) {
            fetch();
        } else {
            new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("네트워크 오류")
                    .setContentText("WI-FI, 모바일 네트워크를 확인해 주세요.")
                    .showCancelButton(false)
                    .setCancelClickListener(null)
                    .setConfirmText("확인")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            finish();
                        }
                    })
                    .show();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    /**
     *  mLocationCallback
     */

    LocationCallback mLocationCallback = new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                if (location != null) {
                    mFusedLocationClient.removeLocationUpdates(this);
                    mLocation = location;
                    Log.d("mLocation", "getLongitude: " + location.getLongitude() + ", getLatitude:" + location.getLatitude());
                    oGeo = new GeoTransPoint(location.getLongitude(), location.getLatitude());
                    oTm = GeoTrans.convert(GeoTrans.GEO, GeoTrans.TM, oGeo);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                GetAPIData reqestStation = new GetAPIData(oTm.getX(), oTm.getY());
                                NowLocationStation = reqestStation.getNearStationAPI();

                                GetAPIData requestDust = new GetAPIData(NowLocationStation);
                                NowLocationData = requestDust.getNowDustAPI();

                                getFavorit(MyFavorit);

                                moveToMain(mLocation.getLatitude(), mLocation.getLongitude());

                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    break;
                }
            }
        }

    };

    public void getFavorit(ArrayList<LocationData> MyFavorit){
        MyFavoritData = new ArrayList<>();
        MyFavoritStation = new ArrayList<>();
        SupportStation_FAV = new ArrayList<>();

        for(int i=0; i<MyFavorit.size();i++){
            try{
                GetAPIData reqestStation = new GetAPIData(Double.parseDouble(MyFavorit.get(i).getTmX()), Double.parseDouble(MyFavorit.get(i).getTmY()));

                GetAPIData requestDust = new GetAPIData(reqestStation.getNearStationAPI());

                MyFavoritData.add(requestDust.addFavoritDustAPI(i));
                MyFavoritStation.add(requestDust.getFavoritNearStation());
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    private void moveToMain(double lat, double lng) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("Longtitude", lng);
        intent.putExtra("Latitude", lat);
        intent.putExtra("MyFavorit", MyFavorit);
        intent.putExtra("NowLocationData", NowLocationData);
        intent.putExtra("NowLocationStation", NowLocationStation);
        intent.putExtra("MyFavoritData", MyFavoritData);
        intent.putExtra("MyFavoritStation", MyFavoritStation);
        intent.putExtra("SupportStation_FAV", SupportStation_FAV);
        intent.putExtra("SupportStation_NOW", SupportStation_NOW);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        FirebaseMessaging.getInstance().subscribeToTopic("notice");

        progress_icon = (ImageView) findViewById(R.id.progress_icon);

        AnimationDrawable animationDrawable = (AnimationDrawable) progress_icon.getDrawable();
        animationDrawable.start();

        data_saver = getSharedPreferences("data_saver", Activity.MODE_PRIVATE);
        favoritManager = new FavoritManager(this, "my_favorit", MyFavorit);
        MyFavorit = new ArrayList<>();
        NowLocationData = new ArrayList<>();
        NowLocationStation = new ArrayList<>();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }

    private void getLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            getCurrentLocation();
        }
    }

    @SuppressLint("MissingPermission")
    public void getCurrentLocation() {
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(120000);
            mLocationRequest.setFastestInterval(12000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
        } else {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("GPS 오류")
                    .setContentText("설정에서 위치정보 사용을 체크 해주세요.")
                    .showCancelButton(false)
                    .setCancelClickListener(null)
                    .setConfirmText("확인")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .show();
        }

    }

    // 퍼미션 결과 처리
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocationPermission();
                } else {
                    finish();
                }
                break;
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    // 네트워크 체크
    public static boolean NetworkConnection(Context context){
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isMobileConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        boolean isWifiAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
        boolean isWifiConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        if((isWifiAvailable && isWifiConnect) || (isMobileConnect)) {
            return true;
        } else {
            return false;
        }
    }

    private void fetch() {
        long cacheExpiration = 1;
        if (remoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        remoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            remoteConfig.activateFetched();
                        }

                        // Firebase
                        FirebaseMessaging.getInstance().subscribeToTopic("notice");

                        try {
                            deviceVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

                            String version = remoteConfig.getString(Global.KEY_VERSION_CODE);
                            MARKET_URL = remoteConfig.getString(Global.KEY_MARKET_URL);

                            if (version.compareTo(deviceVersion) > 0) {
                                new SweetAlertDialog(SplashActivity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                        .setCustomImage(R.drawable.ic_cloud_download)
                                        .setTitleText("업데이트")
                                        .setContentText("새로운 버전으로 업데이트 하시겠습니까?")
                                        .showCancelButton(true)
                                        .setCancelText("취소")
                                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                                MyFavorit = favoritManager.getFavorit();

                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        getLocationPermission();
                                                    }
                                                }, SPLASH_DISPLAY_LENGTH);
                                            }
                                        })
                                        .setConfirmText("업데이트")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismiss();
                                                Intent marketLaunch = new Intent(
                                                        Intent.ACTION_VIEW);
                                                marketLaunch.setData(Uri.parse(MARKET_URL));
                                                startActivity(marketLaunch);
                                                finish();
                                            }
                                        })
                                        .show();
                            }else{
                                MyFavorit = favoritManager.getFavorit();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        getLocationPermission();
                                    }
                                }, SPLASH_DISPLAY_LENGTH);
                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

}
